#let accent_color = red
#let table_header_color = accent_color.lighten(70%)
#let dark_table_row_color = luma(240)
#let light_table_row_color = luma(250)
#let table_base = table.with(stroke: none)
#let simple_table = table_base.with(
  fill: (col, row) =>
    if calc.even(row) {
      dark_table_row_color
    } else {
      light_table_row_color
    }
)
#let headed_table = table_base.with(
  fill: (col, row) =>
    if row == 0 {
      table_header_color
    } else if calc.odd(row) {
      dark_table_row_color
    } else {
      light_table_row_color
    }
)
#let kv_table = table.with(
  inset: 1pt,
  stroke: none,
  gutter: 5pt,
  columns: 2,
  align: (right, left)
)
#let template(content, outlink_icon: none) = [
  #set page(margin: 4em)
  #set text(
    lang: "de",
    font: "New Computer Modern Sans"
  )
  #show link: it => {
    underline(stroke: (dash: "dashed"), it)
    [ ]
    if outlink_icon != none {
      box(image("outlink.svg", height: .7em))
    }
  }
  #set list(marker: [--])
  #content
]
